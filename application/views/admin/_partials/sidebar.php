<!-- Sidebar -->
<ul class="sidebar navbar-nav">
    <li class="nav-item <?php echo $this->uri->segment(2) == '' ? 'active': '' ?>">
        <a class="nav-link" href="<?php echo site_url('admin') ?>">
            <i class="fas fa-fw fa-tachometer-alt"></i>
            <span>Overview</span>
        </a>
    </li>
    <li class="nav-item dropdown <?php echo $this->uri->segment(2) == 'products' ? 'active': '' ?>">
        <a class="nav-link dropdown-toggle" href="#" id="pagesDropdown" role="button" data-toggle="dropdown" aria-haspopup="true"
            aria-expanded="false">
            <i class="fas fa-fw fa-boxes"></i>
            <span>Products</span>
        </a>
        <div class="dropdown-menu" aria-labelledby="pagesDropdown">
            <a class="dropdown-item" href="<?php echo site_url('admin/products/add') ?>">New Product</a>
            <a class="dropdown-item" href="<?php echo site_url('admin/products') ?>">List Product</a>
        </div>
    </li>
    <li class="nav-item">
        <a class="nav-link" href="#">
            <i class="fas fa-fw fa-users"></i>
            <span>Users</span></a>
    </li>
    <li class="nav-item">
        <a class="nav-link" href="#">
            <i class="fas fa-fw fa-cog"></i>
            <span>Settings</span></a>
    </li>



 <?php
    // data main menu
    $main_menu = $this->db->get_where('tabel_menu', array('is_main_menu' => 0));
        foreach ($main_menu->result() as $main) {

            // Query untuk mencari data sub menu
        $sub_menu = $this->db->get_where('tabel_menu', array('is_main_menu' => $main->id));

            // periksa apakah ada sub menu
        if ($sub_menu->num_rows() > 0) {

           // main menu dengan sub menu
            echo "
            <li class= 'nav-item dropdown'>
            <a class='nav-link dropdown-toggle' href='#' id='pagesDropdown' role='button' data-toggle='dropdown' aria-haspopup='true'
            aria-expanded='false'>
            <i class='fas fa-fw fa-boxes'></i>
            <span>".$main->judul_menu."</span>
           </a>
            ";

            // sub menu disini
            foreach ($sub_menu->result() as $sub) {
            echo "<div class='dropdown-menu' aria-labelledby='pagesDropdown'>
            <a class='dropdown-item' href=".$sub->link.">".$sub->judul_menu."</a>
            <a class='dropdown-item' href=".$sub->link.">".$sub->judul_menu."</a>
           </div>
            ";
            }
            echo "</li>"; 
            } 

            else {

            // main menu tanpa sub menu
            echo "<li class='nav-item'>
                <a class='nav-link href=".$main->link." <i class='fas fa-fw fa-cog'</i>
                <span>".$main->judul_menu."</span></a></li>";
             }
    }
                       
    ?>

</ul>

